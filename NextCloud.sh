#!/bin/bash
#Step 1: Install nginx on Ubuntu 20.04
sudo apt update && sudo apt upgrade -y
sudo apt install nodejs npm -y
sudo apt install -y nginx && sudo systemctl enable nginx
sudo systemctl start nginx
#Step 2: Download NextCloud on Ubuntu 20.04
wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
sudo apt install unzip
sudo unzip nextcloud-21.0.1.zip -d /usr/share/nginx/
sudo chown www-data:www-data /usr/share/nginx/nextcloud/ -R
#Step 2: Create a Database and User for Nextcloud in MariaDB Database Server
sudo apt install mysql-server -y
sudo mysql -s <<EOF
create database nextcloud;
create user nextclouduser@localhost identified by 'nextcloud';
grant all privileges on nextcloud.* to nextclouduser@localhost;
flush privileges;
exit
EOF
#Step 3: Create a Nginx Config File for Nextcloud
#sudo nano /etc/nginx/conf.d/nextcloud.conf
sudo chmod a+rw /etc/nginx/conf.d/
sudo touch /etc/nginx/conf.d/nextcloud.conf
sudo rm -rf /etc/nginx/conf.d/nextcloud.conf
sudo mv -f nextcloud.conf /etc/nginx/conf.d
#cat nextcloud.conf >> /etc/nginx/sites-available/nextcloud.conf  -y
sudo systemctl reload nginx
sudo apt-get update && sudo apt-get upgrade -y 
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:ondrej/php  -y
sudo apt update
sudo apt install php7.4 -y
sudo apt install imagemagick php-imagick php7.4-common php7.4-mysql php7.4-fpm php7.4-gd php7.4-json php7.4-curl  php7.4-zip php7.4-xml php7.4-mbstring php7.4-bz2 php7.4-intl php7.4-bcmath php7.4-gmp -y
echo "L'installation de nextcloud est terminée, pour faire le test MyIP:9000"
